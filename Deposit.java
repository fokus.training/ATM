package com.ATM;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Deposit {

    public String user_login;
    public int deposit;
    
    public Deposit(String user_login, int deposit){
    	this.user_login = user_login;
    	this.deposit = deposit;    	
    }
    
	public String doDeposit() {
		String string = "";
    	JSONParser parser = new JSONParser();    	
	    int lastdeposit = 0;
	    JSONObject countryObj = new JSONObject();
	    String owedto = "";
	    int Balance = 0;
		try {
			JSONArray listOfStates = (JSONArray) parser.parse(new FileReader("atm.json"));						
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(this.user_login)) {
					Balance = Integer.parseInt(countryObj.get("Balance").toString());
					JSONObject cO = (JSONObject) countryObj.get("OwedTo");
				    for (Object key : cO.keySet()) {
				        String keyStr = (String)key;
				        int keyvalue = Integer.parseInt(cO.get(keyStr).toString());
						for (Object log2 : listOfStates) {
							countryObj = (JSONObject)log2;
							if(countryObj.get("Name").equals(keyStr)) {
								lastdeposit = this.deposit;
								this.deposit = this.deposit - keyvalue;
								if(this.deposit >=0 ) {
									string += "Transferred $"+keyvalue + " to " + keyStr;																																									
									cO.remove(keyStr);
									countryObj.put("Balance", Integer.parseInt(countryObj.get("Balance").toString()) + keyvalue);
								}else {
									cO.put(keyStr,keyvalue-lastdeposit);
									owedto += "Owed $"+(keyvalue-lastdeposit)+" to "+keyStr;
									string += "Transferred $"+ lastdeposit + " to " + keyStr;														
									countryObj.put("Balance", Integer.parseInt(countryObj.get("Balance").toString()) + lastdeposit);
									this.deposit = 0;														
									break;
								}
							}
						}									        
				    }										
				}							
			}	
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(this.user_login)) {
					Balance = Integer.parseInt(countryObj.get("Balance").toString()) + this.deposit;
					countryObj.put("Balance", Balance);
					break;
				}							
			}								
	        Helper.writingFile(listOfStates);																					        
	    } catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}												  
		string += "\r\n"+"your balance is $"+Balance;
		if(!owedto.equals("")) {
			string += "\r\n";
			string += owedto;  					
		}
		return string;
	}
}
