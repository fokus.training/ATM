package com.ATM;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;

public class Helper {
	static void writingFile(JSONArray listOfStates) {
        try {  
            // Writing to a file  
            File file=new File("atm.json");  
            file.createNewFile();  
            FileWriter fileWriter = new FileWriter(file);  
            fileWriter.write(listOfStates.toJSONString());  
            fileWriter.flush();  
            fileWriter.close();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }		
	}
}
