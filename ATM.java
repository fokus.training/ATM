package com.ATM;
import java.util.Scanner;

public class ATM {
    public String command;
    public String user_login;
    
    public ATM(){
    	
    }
    
	public String runningCommand(String command) {
		String parts_input = "";
		String input = command;  // Read user input
		String[] parts = input.split(" ");
		switch(parts[0]) {
			case "login":
				    parts_input = parts[0];
				    this.user_login = parts[1];
				    Login login = new Login(this.user_login);
				    return login.doLogin();
			  case "deposit":
				    Deposit deposit = new Deposit(this.user_login,Integer.parseInt(parts[1]));
				    return deposit.doDeposit();
			  case "transfer":
				  Transfer transfer = new Transfer(this.user_login,parts[1],parts[2]);
				  return transfer.doTransfer();
			  case "logout":
				  parts_input = "logout";						  						  
				  Logout logout = new Logout(this.user_login);
				  return logout.doLogout();					  
		}
	    return "";
	}    
    
	public static void main(String[] args) {		
		String parts_input = "";
		String user_login = "";
	    try (Scanner myObj = new Scanner(System.in)) {
	    	while (parts_input != "logout") {
				String input = myObj.nextLine();  // Read user input
				String[] parts = input.split(" ");
				switch(parts[0]) {
					  case "login":
						    parts_input = parts[0];
						    user_login = parts[1];
						    Login login = new Login(user_login);
						    login.doLogin();
					  break;
					  case "deposit":
						    Deposit deposit = new Deposit(user_login,Integer.parseInt(parts[1]));
						    deposit.doDeposit();
					  break;	  
					  case "transfer":
						  Transfer transfer = new Transfer(user_login,parts[1],parts[2]);
						  transfer.doTransfer();						  
					  break;					  
					  case "logout":
						  parts_input = "logout";						  						  
						  Logout logout = new Logout(user_login);
						  logout.doLogout();						  
					  break;					  
				}
	    	}
		}	    

	}

}
