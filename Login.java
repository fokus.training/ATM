package com.ATM;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Login {

    public String username;
    
    public Login(String username){
        this.username = username;
    }	
		
    public String doLogin() {
		String string = "";
    	JSONParser parser = new JSONParser();  
    	JSONObject countryObj = new JSONObject();
    	Integer Balance = 0;
		String owed = "";
	    String user_login = this.username;
		try {
			Boolean checkBalanceName = false;
			JSONArray listOfStates = (JSONArray) parser.parse(new FileReader("atm.json"));
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(user_login)) {
					Balance = Integer.parseInt(countryObj.get("Balance").toString());
					JSONObject cO = (JSONObject) countryObj.get("OwedTo");
				    for (Object key : cO.keySet()) {
				        String keyStr = (String)key;
				        Object keyvalue = cO.get(keyStr);
				        owed=owed + "Owed $"+ keyvalue + " to " + keyStr+"\r\n";
				        	
				    }										
					checkBalanceName = true;
				}else if(!countryObj.get("Name").equals(user_login)){
					JSONObject cO = (JSONObject) countryObj.get("OwedTo");
					if(cO.get(user_login) != null)
					owed=owed + "Owed $" +cO.get(user_login)+" from "+countryObj.get("Name")+"\r\n";
				}									
			}
			if(!checkBalanceName) {
				listOfStates = (JSONArray) parser.parse(new FileReader("atm.json"));						
				countryObj.put("Name", username);  
				countryObj.put("Balance", new Integer(0));
				countryObj.put("OwedTo", new JSONObject());
				listOfStates.add(countryObj);
				Helper.writingFile(listOfStates);										
			}					        
		} catch (FileNotFoundException e) {
	        countryObj.put("Name", username);  
	        countryObj.put("Balance", new Integer(0));
			countryObj.put("OwedTo", new JSONObject());  
	        JSONArray listOfStates = new JSONArray(); 
	        listOfStates.add(countryObj);								
	        Helper.writingFile(listOfStates);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) { 
	        countryObj.put("Name", username);  
	        countryObj.put("Balance", new Integer(0));
			countryObj.put("OwedTo", new JSONObject());  
	        JSONArray listOfStates = new JSONArray(); 
	        listOfStates.add(countryObj);								
	        Helper.writingFile(listOfStates);							
	    }
		string += "Hello, "+username+"!\r\n";
		string += "Your balance is $"+Balance+"\r\n";
		string += owed+"\r\n";
		return string;
	}
}
