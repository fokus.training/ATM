package com.ATM;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestUnitRunner {
   public static void main(String[] args) {
	  Result result = JUnitCore.runClasses(TestJunit.class);	
      for (Failure failure : result.getFailures()) {
         System.out.println("Failure: "+failure.toString());
      }
      if(result.wasSuccessful()) {    	  
          System.out.println("All Tests are passed");    	  
      }
   }
} 