package com.ATM;

import org.junit.Test;

import junit.framework.TestCase;

public class TestJunit extends TestCase  {
	
	public static String capitalizeString(String string) {
		  char[] chars = string.toLowerCase().toCharArray();
		  boolean found = false;
		  for (int i = 0; i < chars.length; i++) {
		    if (!found && Character.isLetter(chars[i])) {
		      chars[i] = Character.toUpperCase(chars[i]);
		      found = true;
		    } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
	      found = false;
	    }
	  }
	  return String.valueOf(chars);
	}	
	@Test
   public void testAdd() {
	  String str = "";
	  String expected = "";
	  String result = "";
	  ATM atm = new ATM();
	  str = "login Alice";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Hello, Alice!\r\n"
	  		          + "Your balance is $0\r\n";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "deposit 100";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Your balance is $100";
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "logout";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Goodbye, Alice!";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "login Bob";
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Hello, Bob!\r\n"
	  		+ "Your balance is $0";	  
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "deposit 80";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Your balance is $80";	  
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "transfer Alice 50";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Transferred $50 to Alice\r\n"
	  		+ "Your balance is $30";	  
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "transfer Alice 100";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Transferred $30 to Alice\r\n"
	  		+ "Your balance is $0\r\n"
	  		+ "Owed $70 to Alice";	  
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "deposit 30";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Transferred $30 to Alice\r\n"
	  		+ "Your balance is $0\r\n"
	  		+ "Owed $40 to Alice";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n\r\n"+result+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "logout";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Goodbye, Bob!";	
	  result = atm.runningCommand(str);
	  str = "login Alice";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Hello, Alice!\r\n"
	  		+ "Your balance is $210\r\n"
	  		+ "Owed $40 from Bob";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "transfer Bob 30";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Transferred $30 to Bob\r\n"
	  		   + "Your balance is $210\r\n"
	  		   + "Owed $10 from Bob";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "logout";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Goodbye, Alice!";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "login Bob";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Hello, Bob!\r\n"
	  		+ "Your balance is $0\r\n"
	  		+ "Owed $10 to Alice";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "deposit 100";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Transferred $10 to Alice\r\n"
	  		+ "Your balance is $90";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
	  str = "logout";
	  System.out.print("Command : \r\n"+str+"\r\n");
	  expected = "Goodbye, Bob!";	
	  result = atm.runningCommand(str);
	  System.out.print("Result : \r\n"+capitalizeString(result)+"\r\n");	  
	  System.out.print("Expected : \r\n"+capitalizeString(expected)+"\r\n");	  	  	  
	  assertEquals(expected.trim().replaceAll("(\r\n|\n)","").toLowerCase(), result.trim().replaceAll("(\r\n|\n)","").toLowerCase());	  	  	  
   }
    
}