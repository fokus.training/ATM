package com.ATM;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Transfer {
 
	public String user_name;
	public String user_login;
	public String deposit;
    
    public Transfer(String user_login,String user_name, String deposit){
    	this.user_login = user_login;
    	this.user_name = user_name;
    	this.deposit = deposit;    	
    }
    	
    public String doTransfer() {
    	String string = "";
    	JSONParser parser = new JSONParser();       
	    JSONObject countryObj = new JSONObject();  
	    String owedto = "";	  
	    String owed = "";	
	    int Balance = 0;
		try {
			int amount = Integer.parseInt(this.deposit);
			JSONArray listOfStates = (JSONArray) parser.parse(new FileReader("atm.json"));
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(this.user_name)) {
					JSONObject cO = (JSONObject) countryObj.get("OwedTo");
					if(cO.get(this.user_login) != null) {
						int owdFrom = Integer.parseInt(cO.get(this.user_login).toString());
						amount = owdFrom - amount;
						if(amount <= 0) {
							amount = amount * -1;											
							cO.remove(this.user_login);																				
						//	owed = owed + "Owed $0 from "+countryObj.get("Name")+"\r\n";																													
						}else {
							cO.put(this.user_login, amount);																															
							owed = owed + "Owed $"+amount+" from "+countryObj.get("Name")+"\r\n";
							amount = 0;
						}
					}
				}								
			}							
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(this.user_login)) {
					Balance = Integer.parseInt(countryObj.get("Balance").toString()) - amount;
					if(Balance < 0) {
						owedto = owedto + "Owed $"+(Balance * -1)+" to "+this.user_name;
						JSONObject cO = (JSONObject) countryObj.get("OwedTo");
						int balanceCurrentUser = 0;
						if(cO.get(this.user_name) != null) {   
							balanceCurrentUser = Integer.parseInt(cO.get(this.user_name).toString());
						}
						amount = amount - ((Balance * -1) + balanceCurrentUser);
						cO.put(this.user_name, (Balance * -1) + balanceCurrentUser);
						deposit = countryObj.get("Balance").toString(); 											
						Balance = 0;
					}											
					countryObj.put("Balance", Balance);
				}								
			}
			for (Object log : listOfStates) {
				countryObj = (JSONObject)log;
				if(countryObj.get("Name").equals(this.user_name)) {
					amount = Integer.parseInt(countryObj.get("Balance").toString()) + amount;
					countryObj.put("Balance", amount);
				}
			}
			Helper.writingFile(listOfStates);
			string += "Transferred $"+this.deposit+" to "+this.user_name;
			string += "\r\n"+"your balance is $"+Balance+"\r\n";	
			if(owedto!="") {
				string += owedto;															
			}
			if(owed!="") {
				string += owed;															
			}							
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return string;    
    }
}
