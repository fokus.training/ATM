package com.ATM;

public class Logout {
    
	public String username;
    
    public Logout(String username){
        this.username = username;
    }	
		
	public String doLogout() {
		return "Goodbye, "+this.username+"!";
	}

}
